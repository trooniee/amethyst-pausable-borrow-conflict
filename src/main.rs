use amethyst::{
    core::SystemDesc,
    derive::SystemDesc,
    core::transform::TransformBundle,
    ecs::prelude::{ReadExpect, SystemData, Write, System},
    prelude::*,
    renderer::{
        plugins::{RenderFlat2D, RenderToWindow},
        types::DefaultBackend,
        RenderingBundle,
    },
    utils::application_root_dir,
};

struct MyState;

#[derive(Default, PartialEq)]
struct MyResource;

#[derive(SystemDesc)]
struct MySystem;

impl<'s> System<'s> for MySystem {
    type SystemData = Write<'s, MyResource>;
    fn run(&mut self, data: Self::SystemData) {}
}

impl SimpleState for MyState {
    fn on_start(&mut self, _data: StateData<'_, GameData<'_, '_>>) {}
}

fn main() -> amethyst::Result<()> {
    amethyst::start_logger(Default::default());

    let app_root = application_root_dir()?;

    let config_dir = app_root.join("config");
    let display_config_path = config_dir.join("display.ron");

    let game_data = GameDataBuilder::default()
        .with_bundle(
            RenderingBundle::<DefaultBackend>::new()
                .with_plugin(
                    RenderToWindow::from_config_path(display_config_path)
                        .with_clear([0.34, 0.36, 0.52, 1.0]),
                )
                .with_plugin(RenderFlat2D::default()),
        )?
        .with_bundle(TransformBundle::new())?
        .with(MySystem.pausable(MyResource), "my_system", &[]);

    let mut game = Application::new("/", MyState, game_data)?;
    game.run();

    Ok(())
}
